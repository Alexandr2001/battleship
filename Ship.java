import java.util.*;

public class Ship {

    public static final String GOT = "ПОПАЛ";
    public static final String MIMO = "ПРОМАХ";
    private static final String SUNK = "ПОТОПЛЕН";
    public static final String ALREADY_SHOT = "ВЫ УЖЕ СЮДА ПОПАДАЛИ! ";
    public static ArrayList coordinateShot = new ArrayList();

    public static ArrayList<Integer> ships = new ArrayList<>();

    Ship() {
        int x = (int)(Math.random() * 9);
        ships.add(x);
        ships.add(x + 1);
        ships.add(x + 2);
    }

    static boolean isAlive() {
        if (ships.isEmpty()){
            System.out.println(SUNK);
            return true;
        }
        return false;
    }
}