import java.util.Scanner;

public class Demo {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        Ship ship = new Ship();
        int count = game();
        System.out.println("Вы потопили корабль за "+count+" попыток! ");
    }

    private static int game() {
        int count = 3;
        while (!Ship.isAlive()) {
            System.out.print("Сделайте выстрел: ");
            int shot = scan.nextInt();
            chanel(shot);
            if(Ship.coordinateShot.contains(shot)){
                System.out.println(Ship.ALREADY_SHOT);
            }else{
                if (!Ship.ships.contains(shot)){
                    System.out.println(Ship.MIMO);
                    Ship.coordinateShot.add(shot);
                    count ++;
                }else{
                    int y = Ship.ships.indexOf(shot);
                    Ship.ships.remove(y);
                    Ship.coordinateShot.add(shot);
                    System.out.println(Ship.GOT);
                }
            }
        }
        return count;
    }

    private static void chanel(int shot) {
        while (shot < 0 || shot > 11) {
            System.out.println("Введите от 0 - 11");
            shot = scan.nextInt();
        }
    }
}